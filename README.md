<img src="misc/spadefish.svg" />

This repository is for my master thesis in computer science and contains the experimental changes I've made as a reference to extract as much knowledge from these changes as possible.
If you are looking for the main Spade repo - [go here](https://gitlab.com/spade-lang/spade).

There are a set of different changes, some with minute differences. This is covered in the thesis and I deligate you to read more there. But the branches of interest are:

```
  the-simplest-implementation
  the-simplest-implementation-for-int-size
  the-second-simplest-thing
  the-third-simplest-with-equations
  the-fourth-attempt-now-with-equivelence
  the-fifth-attempt-now-without-returns
  the-sixth-attempt
  the-seventh-attempt-almost-as-simple-as-attempt-one
```

